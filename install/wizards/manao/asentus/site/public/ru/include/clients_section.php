<div class="bg-color-sky-light">
	<div class="content-lg container">
		<div class="swiper-slider swiper-clients">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
 <img alt="Clients Logo" src="<?=SITE_TEMPLATE_PATH?>/img/clients/01.png" class="swiper-clients-img">
				</div>
				<div class="swiper-slide">
 <img alt="Clients Logo" src="<?=SITE_TEMPLATE_PATH?>/img/clients/02.png" class="swiper-clients-img">
				</div>
				<div class="swiper-slide">
 <img alt="Clients Logo" src="<?=SITE_TEMPLATE_PATH?>/img/clients/03.png" class="swiper-clients-img">
				</div>
				<div class="swiper-slide">
 <img alt="Clients Logo" src="<?=SITE_TEMPLATE_PATH?>/img/clients/04.png" class="swiper-clients-img">
				</div>
				<div class="swiper-slide">
 <img alt="Clients Logo" src="<?=SITE_TEMPLATE_PATH?>/img/clients/05.png" class="swiper-clients-img">
				</div>
				<div class="swiper-slide">
 <img alt="Clients Logo" src="<?=SITE_TEMPLATE_PATH?>/img/clients/06.png" class="swiper-clients-img">
				</div>
			</div>
		</div>
	</div>
</div>
<br>