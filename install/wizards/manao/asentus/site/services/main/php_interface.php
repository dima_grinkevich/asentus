<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

if (!defined("WIZARD_SITE_ID"))
    return;
	
CWizardUtil::ReplaceMacros(WIZARD_ABSOLUTE_PATH . "/site/bitrix_php_interface/". LANGUAGE_ID . "/init.php", array(
	"TEMPLATE_NAME" => GetMessage("wis_template_name")
));

$bitrixPhpInterfaceDir = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/' . WIZARD_SITE_ID;

CopyDirFiles(
    WIZARD_ABSOLUTE_PATH . "/site/bitrix_php_interface/". LANGUAGE_ID . "/",
    $bitrixPhpInterfaceDir,
    $rewrite = true,
    $recursive = true,
    $delete_after_copy = false
);
?>
