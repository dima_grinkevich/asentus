<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

$arEventTypes = Array();
$langs = CLanguage::GetList(($b=""), ($o=""));
while($language = $langs->Fetch())
{
	$lid = $language["LID"];
	IncludeModuleLangFile(__FILE__, $lid);
}

IncludeModuleLangFile(__FILE__);

CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/_index.php", 
	array(
		"MAIL" => htmlspecialcharsbx($wizard->GetVar("admins_e_mail")),
		"OK_TEXT" => GetMessage('OK_TEXT'),
		)
	);
?>