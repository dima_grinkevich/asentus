<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"DISPLAY_PICTURE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_PICTURE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PREVIEW_TEXT" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_TEXT"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"USE_SHARE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_USE_SHARE"),
		"TYPE" => "CHECKBOX",
		"MULTIPLE" => "N",
		"VALUE" => "Y",
		"DEFAULT" =>"N",
		"REFRESH"=> "Y",
	),
    "BLOCK_TITLE" => Array(
        "NAME" => GetMessage("T_IBLOCK_DESC_NEWS_BLOCK_TITLE"),
        "TYPE" => "STRING",
        "DEFAULT" => "Our Exceptional Solutions",
    ),
    "BLOCK_DESCRIPTION" => Array(
        "NAME" => GetMessage("T_IBLOCK_DESC_NEWS_BLOCK_DESCRIPTION"),
        "TYPE" => "STRING",
        "DEFAULT" => "Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incididunt ut laboret dolore magnaaliqua enim minim veniam exercitation",
    ),
);
?>