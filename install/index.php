<?
IncludeModuleLangFile(__FILE__);
Class manao_asentus extends CModule
{
	const MODULE_ID = 'manao.asentus';
	var $MODULE_ID = "manao.asentus";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;

	function __construct()
	{
		$arModuleVersion = array();

		include(dirname(__FILE__)."/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

		$this->MODULE_NAME = GetMessage("MANAO_ASENTUS_INSTALL_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("MANAO_ASENTUS_INSTALL_DESCRIPTION");
		$this->PARTNER_NAME = GetMessage("MANAO_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("MANAO_PARTNER_URI");
	}


	function InstallDB($install_wizard = true)
	{
		RegisterModule(self::MODULE_ID);
		return true;
	}

	function UnInstallDB($arParams = Array())
	{
		UnRegisterModule(self::MODULE_ID);
		return true;
	}

	function InstallFiles()
	{
		CopyDirFiles(
            $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/components",
            $_SERVER["DOCUMENT_ROOT"]."/bitrix/components/", true, true
        );
		return true;
	}

	function UnInstallFiles()
	{
		DeleteDirFilesEx(
            $_SERVER["DOCUMENT_ROOT"]."/bitrix/components/".$this->MODULE_ID."/"
        );
		return true;
	}

	function DoInstall()
	{
        $this->InstallDB();
        $this->InstallFiles();
    }

	function DoUninstall()
	{
        $this->UnInstallDB();
        $this->UnInstallFiles();
    }
}
?>
